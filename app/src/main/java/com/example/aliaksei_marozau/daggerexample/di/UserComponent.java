package com.example.aliaksei_marozau.daggerexample.di;

import com.example.aliaksei_marozau.daggerexample.SessionManager;
import com.example.aliaksei_marozau.daggerexample.ui.RootUserUiModule;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;

// use import dagger.android.___support___.DaggerApplication;
@UserScope
@Subcomponent(modules = {
        UserModule.class,
        RootUserUiModule.class,
})
public interface UserComponent extends AndroidInjector<DaggerApplication> {
    void inject(SessionManager sessionManager);

    @Subcomponent.Builder
    interface Builder {

        UserComponent build();
    }

}