package com.example.aliaksei_marozau.daggerexample.di;

import android.app.Application;

import com.example.aliaksei_marozau.daggerexample.ExampleApp;

import dagger.Module;
import dagger.Provides;

@Module(subcomponents = UserComponent.class)
public class AppModule {

    @Provides
    Application provideContext(ExampleApp application) {
        return application;
    }
}