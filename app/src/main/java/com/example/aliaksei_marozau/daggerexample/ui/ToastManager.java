package com.example.aliaksei_marozau.daggerexample.ui;

import android.app.Application;
import android.content.Context;
import android.widget.Toast;

import javax.inject.Inject;
import javax.inject.Singleton;

import timber.log.Timber;

@Singleton
public class ToastManager {

    private Context context;

    @Inject
    public ToastManager(Application application) {
        this.context = application;
        Timber.d("Create ToastManager");
    }

    public void show(String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }
}
