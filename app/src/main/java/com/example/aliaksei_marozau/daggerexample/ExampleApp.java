package com.example.aliaksei_marozau.daggerexample;

import android.app.Activity;
import android.support.v4.app.Fragment;

import com.example.aliaksei_marozau.daggerexample.di.DaggerAppComponent;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.DaggerApplication;
import timber.log.Timber;

// use import dagger.android.___support___.DaggerApplication;
// DaggerApplication has DispatchingAndroidInjector for all android types
public class ExampleApp extends DaggerApplication {

    @Inject SessionManager sessionManager;

    @Override
    public void onCreate() {
        super.onCreate();

        // create fake login because we used api on main screen
        sessionManager.createUserSession();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerAppComponent.builder().create(this);
    }

    @Override
    public DispatchingAndroidInjector<Activity> activityInjector() {
        return sessionManager.activityInjector();
    }

    // if we used scoped objects from fragment we need scoped fragment injector too
    @Override
    public DispatchingAndroidInjector<Fragment> supportFragmentInjector() {
        return sessionManager.supportFragmentInjector();
    }
}