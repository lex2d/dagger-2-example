package com.example.aliaksei_marozau.daggerexample.di;

import com.example.aliaksei_marozau.daggerexample.ExampleApp;
import com.example.aliaksei_marozau.daggerexample.ui.RootUiModule;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {
        /* Use AndroidInjectionModule.class if you're not using support library */
        AndroidSupportInjectionModule.class,
        AppModule.class,
        RootUiModule.class
})
public interface AppComponent extends AndroidInjector<ExampleApp> {

    @Component.Builder
    abstract class Builder extends AndroidInjector.Builder<ExampleApp> {
    }
}