package com.example.aliaksei_marozau.daggerexample.ui;

import dagger.Module;

// For module that dont use @UserScope
@Module(includes = {
})
public abstract class RootUiModule {
}
