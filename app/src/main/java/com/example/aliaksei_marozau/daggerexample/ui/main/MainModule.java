package com.example.aliaksei_marozau.daggerexample.ui.main;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class MainModule {

//    @ActivityScope
    @ContributesAndroidInjector()
    abstract MainActivity contributeMainActivityInjector();

    @ContributesAndroidInjector()
    abstract MainActivityFragment contributeMainActivityFragmentInjector();
}
