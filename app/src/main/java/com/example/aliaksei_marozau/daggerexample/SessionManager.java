package com.example.aliaksei_marozau.daggerexample;

import android.app.Activity;
import android.support.v4.app.Fragment;

import com.example.aliaksei_marozau.daggerexample.di.UserComponent;

import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import dagger.android.support.HasSupportFragmentInjector;

@Singleton
public class SessionManager implements HasActivityInjector, HasSupportFragmentInjector {
    // we get this from constructor when inject manager in Application
    private final UserComponent.Builder userComponentBuilder;
    // we init this with default same as in Application injectors during call inject in Application
    // than we reinject this when call createUserSession()
    @Inject DispatchingAndroidInjector<Activity> activityInjector;
    @Inject DispatchingAndroidInjector<Fragment> fragmentInjector;

    private UserComponent userComponent;

    @Inject
    public SessionManager(UserComponent.Builder builder) {
        this.userComponentBuilder = builder;
    }

    public void createUserSession() {
        userComponent = userComponentBuilder.build();
        userComponent.inject(this);
    }

    public boolean hasSession() {
        return userComponent != null;
    }

    // dont set injector to null or we cant inject anything after session end
    public void endUserSession() {
        userComponent = null;
    }

    @Override
    public DispatchingAndroidInjector<Activity> activityInjector() {
        return activityInjector;
    }

    @Override
    public DispatchingAndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentInjector;
    }
}
