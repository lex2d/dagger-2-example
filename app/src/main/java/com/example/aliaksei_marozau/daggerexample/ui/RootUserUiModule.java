package com.example.aliaksei_marozau.daggerexample.ui;

import com.example.aliaksei_marozau.daggerexample.ui.main.MainModule;
import com.example.aliaksei_marozau.daggerexample.ui.second.SecondModule;

import dagger.Module;

@Module(includes = {
        MainModule.class,
        SecondModule.class,
})
public abstract class RootUserUiModule {
}
