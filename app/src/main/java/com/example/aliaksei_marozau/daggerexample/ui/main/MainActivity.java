package com.example.aliaksei_marozau.daggerexample.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.aliaksei_marozau.daggerexample.R;
import com.example.aliaksei_marozau.daggerexample.SessionManager;
import com.example.aliaksei_marozau.daggerexample.data.SomeClientApi;
import com.example.aliaksei_marozau.daggerexample.ui.ToastManager;
import com.example.aliaksei_marozau.daggerexample.ui.second.SecondActivity;

import javax.inject.Inject;

import dagger.android.AndroidInjection;

public class MainActivity extends AppCompatActivity {

    @Inject SessionManager sessionManager;
    @Inject SomeClientApi someClientApi;
    @Inject ToastManager toastManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // after session creation we get new scoped values
                sessionManager.createUserSession();
                startActivity(new Intent(MainActivity.this, SecondActivity.class));
            }
        });

        someClientApi.run();
        toastManager.show("MainActivity");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
