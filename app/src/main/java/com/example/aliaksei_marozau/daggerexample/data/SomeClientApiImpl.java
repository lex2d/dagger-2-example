package com.example.aliaksei_marozau.daggerexample.data;

import timber.log.Timber;

public class SomeClientApiImpl implements SomeClientApi {

    public SomeClientApiImpl() {
        Timber.d("Create SomeClientApiImpl");
    }

    @Override
    public void run() {
        Timber.d("SomeClientApiImpl run");
    }
}
