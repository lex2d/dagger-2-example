package com.example.aliaksei_marozau.daggerexample.ui.second;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class SecondModule {

    //    @ActivityScope
    @ContributesAndroidInjector()
    abstract SecondActivity contributeSecondActivityInjector();

    @ContributesAndroidInjector()
    abstract SecondActivityFragment contributeSecondActivityFragmentInjector();
}
