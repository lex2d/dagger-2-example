package com.example.aliaksei_marozau.daggerexample.di;

import com.example.aliaksei_marozau.daggerexample.data.SomeClientApi;
import com.example.aliaksei_marozau.daggerexample.data.SomeClientApiImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class UserModule {

    @UserScope
    @Provides
    SomeClientApi provideSomeClientApi() {
        return new SomeClientApiImpl();
    }
}